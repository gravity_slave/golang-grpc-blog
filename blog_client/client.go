package main

import (
	"context"
	"fmt"
	"go_blog_rpc/blogpb"
	"google.golang.org/grpc"
	"io"
	"log"
)

func main() {
	fmt.Println("Blog Client")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	client := blogpb.NewBlogServiceClient(cc)

	fmt.Println("Creating blog")

	blog := &blogpb.Blog{
		AuthorId: "Bumazey",
		Title:    "Grpc blog",
		Content:  "Grpc to mongo",
	}
	createBlogRes, err := client.CreateBlog(context.Background(), &blogpb.CreateBlogRequest{Blog: blog})
	if err != nil {
		log.Fatalf("unexpected error: %v", err)
	}

	fmt.Printf("Blog has been created: %v", createBlogRes.GetBlog())
	blogId := createBlogRes.GetBlog().GetId()

	fmt.Println("Reading the blog")

	readBlogReq := &blogpb.ReadBlogRequest{BlogId: blogId}

	readBlogRes, err2 := client.ReadBlog(context.Background(), readBlogReq)
	if err2 != nil {
		fmt.Printf("Error happened while reading: %v", err2)
	}

	fmt.Printf("Blog was read: %v", readBlogRes.GetBlog())

	fmt.Println("Updating blog")
	newBlog := &blogpb.Blog{
		Id:       blogId,
		AuthorId: "Abumazey",
		Title:    "Edited grpc stuff",
		Content:  "lorem ipsum",
	}

	updateRes, updateErr := client.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{Blog: newBlog})
	if updateErr != nil {
		fmt.Printf("Error happened while updating: %v \n", updateErr)
	}

	fmt.Printf("Blog was updated: %v", updateRes.GetBlog())

	// delete Blog
	deleteRes, deleteErr := client.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest{BlogId: blogId})
	if deleteErr != nil {
		fmt.Printf("Error happened while deleting: %v \n", updateErr)
	}
	fmt.Printf("Blog was deleted: %v \n", deleteRes.GetBlogId())

	// list Blogs

	stream, err := client.ListBlog(context.Background(), &blogpb.ListBlogRequest{})
	if err != nil {
		log.Fatalf("error while calling ListBlog RPC: %v", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Something happened: %v", err)
		}
		fmt.Println(res.GetBlog())
	}

}
